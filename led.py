import sys
sys.dont_write_bytecode = True
import time

from naoqi import ALProxy
from naoqi import ALBroker
from naoqi import ALModule


# ----------> Color List <----------
ColorList = ['red', 'white', 'green', 'blue', 'yellow', 'magenta', 'cyan']

#-------------------functions of off/on LEDS-------------------------------------------#
def LED_eyes_ON(ledsProxy):
    ledsProxy.on("FaceLeds")

def LED_eyes_OFF(ledsProxy):
    ledsProxy.off("FaceLeds")

def LED_ear_ON(ledsProxy):
    ledsProxy.on("EarLeds")

def LED_ear_OFF(ledsProxy):
    ledsProxy.off("EarLeds")

def LED_feet_ON(ledsProxy):
    ledsProxy.on("FeetLeds")

def LED_feet_OFF(ledsProxy):
    ledsProxy.off("FeetLeds")

def LED_head_ON(ledsProxy):
    ledsProxy.on("BrainLeds")

def LED_head_OFF(ledsProxy):
    ledsProxy.off("BrainLeds")

def LED_chest_ON(ledsProxy):
    ledsProxy.on("ChestLeds")

def LED_chest_OFF(ledsProxy):
    ledsProxy.off("ChestLeds")

#------------------------functions to set LEDS color-----------------------#

def LED_color_eyes(ledsProxy,color,time):
    ledsProxy.fadeRGB("FaceLeds", color, time)

def LED_color_feet(ledsProxy,color,time):
    ledsProxy.fadeRGB("FeetLeds", color, time)

def LED_color_chest(ledsProxy,color,time):
    ledsProxy.fadeRGB("ChestLeds", color, time)

#--------------------------functions to set fade for LEDS-------------------#

def LED_fade_eyes(ledsProxy,intensity,time):
    ledsProxy.fade("FaceLeds", intensity, time)

def LED_fade_ear(ledsProxy,intensity,time):
    ledsProxy.fade("EarLeds", intensity, time)

def LED_fade_feet(ledsProxy,intensity,time):
    ledsProxy.fade("FeetLeds", intensity, time)

def LED_fade_head(ledsProxy,intensity,time):
    ledsProxy.fade("BrainLeds", intensity, time)

def LED_fade_chest(ledsProxy,intensity,time):
    ledsProxy.fade("ChestLeds", intensity, time)

#-------------------------functions to set leds intensity---------------------#
#intensity = float values between 0 and 1
def LED_intensity_eyes(ledsProxy,intensity):
    ledsProxy.setIntensity("FaceLeds", intensity)

def LED_intensity_ear(ledsProxy,intensity):
    ledsProxy.setIntensity("EarLeds", intensity)

def LED_intensity_feet(ledsProxy,intensity):
    ledsProxy.setIntensity("FeetLeds", intensity)

def LED_intensity_head(ledsProxy,intensity):
    ledsProxy.setIntensity("BrainLeds", intensity)

def LED_intensity_chest(ledsProxy,intensity):
    ledsProxy.setIntensity("ChestLeds", intensity)

#---------------------------------------------------------------------------------#


def led_main(ip, port):

    myBroker_child_3 = ALBroker("myBroker",
       "0.0.0.0",
       0,
       ip,
       port)

if __name__ == "__main__":
    led_main()
    