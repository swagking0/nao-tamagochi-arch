from naoqi import ALProxy
import sys
import time
import speech

def main_sub():
    sys.dont_write_bytecode = True
    data=[]
    ip = "169.254.39.105"
    #speekControl = ALProxy("ALTextToSpeech", ip, 9559)
    asr = ALProxy("ALSpeechRecognition", ip, 9559)
    #speech.speech_main(ip,9559)

    asr.pause(True)
    asr.setLanguage("English")


    vocabulary = ["yes", "no", "please","water usage","Thanks","How"]


    asr.setVocabulary(vocabulary, True)
    asr.subscribe(ip)
    memProxy = ALProxy("ALMemory", ip, 9559)
    memProxy.subscribeToEvent('WordRecognized',ip,'wordRecognized')

    asr.pause(False)

    time.sleep(1)

    asr.unsubscribe(ip)
    data = memProxy.getData("WordRecognized")
    print( "data: %s" % data )
    if "bye" not in data[0]:
        if "How" in data[0]:
            print data[0]
            speech.talk(speekControl,"I am good")
        else:
            speech.talk(speekControl, "Sorry I did not get you")
        data[0] = "bye"
