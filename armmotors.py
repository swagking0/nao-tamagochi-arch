import sys
sys.dont_write_bytecode = True
import time

from naoqi import ALProxy
from naoqi import ALBroker
from naoqi import ALModule


#-------------------------------functions for R/L Hands to close and open--------------------#
def openHand_RHand(armProxy):
    armProxy.openHand('RHand')

def closeHand_RHand(armProxy):
    armProxy.closeHand('RHand')

def openHand_LHand(armProxy):
    armProxy.openHand('LHand')

def closeHand_LHand(armProxy):
    armProxy.closeHand('LHand')

#------------------------------functions for body to set stiffness-----------------------#
###stiffvalue = 0 or 1
def stiff_body(armProxy, stiffvalue):
    armProxy.setStiffnesses("Body", stiffvalue)

def stiff_head(armProxy, stiffvalue):
    armProxy.setStiffnesses("Head", stiffvalue)

def stiff_LHand(armProxy, stiffvalue):
    armProxy.setStiffnesses("LArm", stiffvalue)

def stiff_RHand(armProxy, stiffvalue):
    armProxy.setStiffnesses("RArm", stiffvalue)

def stiff_LLeg(armProxy, stiffvalue):
    armProxy.setStiffnesses("LLeg", stiffvalue)

def stiff_RLeg(armProxy, stiffvalue):
    armProxy.setstiffnesses("RLeg", stiffvalue)

#--------------------------------functions for R\L and head to setangles--------------------#

#example names = ["HeadYaw", "HeadPitch"] for more refer from -> http: http://doc.aldebaran.com/2-1/family/robots/joints_robot.html
#angles angle = [0.2, -0.2]

def setang_body(armProxy, names, angles):
    armProxy.setAngles(names, angles, 0.1)





def armMotor_main(ip, port):

    myBroker_child_4 = ALBroker("myBroker",
       "0.0.0.0",
       0,
       ip,
       port)


if __name__ == "__main__":
    armMotor_main()
    
