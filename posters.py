import sys
sys.dont_write_bytecode = True
import time

from naoqi import ALProxy
from naoqi import ALBroker
from naoqi import ALModule


#--------------------functions to set different basic-posters-----------------#
def poster_stand(postProxy):
    postProxy.goToPosture("Stand", 1.0)

def poster_crouch(postProxy):
    postProxy.goToPosture("Crouch", 1.0)

def poster_sit(postProxy):
    postProxy.goToPosture("Sit", 1.0)

#------------------functions to set different non-basic posters---------------#
def poster_relax(postProxy):
    postProxy.goToPosture("SitRelax", 1.0)





def posters_main(ip,port):

    myBroker_child_5 = ALBroker("myBroker",
       "0.0.0.0",
       0,
       ip,
       port)




if __name__ == "__main__":
    posters_main()
    
