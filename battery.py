import sys
sys.dont_write_bytecode = True
import time

from naoqi import ALProxy
from naoqi import ALBroker
from naoqi import ALModule

def battery_status():
    batteryProxy = ALProxy("ALBattery")
    battery_value = batteryProxy.getBatteryCharge()
    return battery_value


def battery_main(ip, port):
    myBroker_child_2 = ALBroker("myBroker",
       "0.0.0.0",
       0,
       ip,
       port)



if __name__ == "__main__":
    battery_main()
    