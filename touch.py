import sys
sys.dont_write_bytecode = True
import time
import random

import MainArc

from naoqi import ALProxy
from naoqi import ALBroker
from naoqi import ALModule


import argparse

# Global variable to store the ReactToTouch module instance
myBroker_child_1 = None
memory = None
Movements_nao = []
Original_Movements_nao = []
Level_2 = []
Level_3 = []

class ReactToTouch(ALModule):
    """ A simple module able to react
        to touch events.
    """
    def __init__(self, name):
        ALModule.__init__(self, name)
        # No need for IP and port here because
        # we have our Python broker connected to NAOqi broker

        # Create a proxy to ALTextToSpeech for later use
        self.tts = ALProxy("ALTextToSpeech")

        # Subscribe to TouchChanged event:
        global memory
        memory = ALProxy("ALMemory")
        memory.subscribeToEvent("TouchChanged",
            "ReactToTouch",
            "onTouched")

    def onTouched(self, strVarName, value):
        """ This will be called each time a touch
        is detected.

        """
        # Unsubscribe to the event when talking,
        # to avoid repetitions
        memory.unsubscribeToEvent("TouchChanged",
            "ReactToTouch")

        touched_bodies = []
        for p in value:
            if p[1]:
                touched_bodies.append(p[0])

        self.say(touched_bodies)

        # Subscribe again to the event
        memory.subscribeToEvent("TouchChanged",
            "ReactToTouch",
            "onTouched")

    def say(self, bodies):
        if (bodies == []):
            return

        sentence = "My " + bodies[0]

        for b in bodies[0:]:
            sentence = sentence + " and my " + b
            if("LArm" == b):
                Movements_nao.append("4")
            if("RHand" == b):
                Movements_nao.append("5")
            if("LFoot/Bumper/Right" == b):
                Movements_nao.append("6")
            if("RFoot/Bumper/Right" == b ):
                Movements_nao.append("7")
            if("Head/Touch/Rear" == b):
                Movements_nao.append("3")
            if("Head/Touch/Middle" == b):
                Movements_nao.append("2")
            if("Head/Touch/Front" == b):
                Movements_nao.append("1")
        sentence = sentence + " touched."

        if(len(Movements_nao) == len(Original_Movements_nao)):
            #for i in range(len(Original_Movements_nao)):
            if(Original_Movements_nao == Movements_nao):
                print("You made it")
                self.tts.say("You made it")
                Level_2.append("True")
                Level_3.append("True")
            else:
                print("You missed your chance")
                self.tts.say("You missed your chance")
                Level_2.append("False")
                Level_3.append("False")
        else:
            print(' '.join(Movements_nao))

def Easy_Series(speak):
    talk = ALProxy("ALTextToSpeech")
    if(speak == 1):
        print("Touch my Right Hand")
        talk.say("Touch my Right Hand")
        Original_Movements_nao.append("5")
    elif(speak == 2):
        print("Touch my Left Hand")
        talk.say("Touch my Left Hand")
        Original_Movements_nao.append("4")
    elif(speak == 3):
        print("Touch my Right Leg")
        talk.say("Touch my Right Leg")
        Original_Movements_nao.append("7")
    elif(speak == 4):
        print("Touch my Left Leg")
        talk.say("Touch my Left Leg")
        Original_Movements_nao.append("6")
    elif(speak == 5):
        print("Touch my Fore Head")
        talk.say("Touch my Fore Head")
        Original_Movements_nao.append("1")
    elif(speak == 6):
        print("Touch my Middle Head")
        talk.say("Touch my Middle Head")
        Original_Movements_nao.append("2")
    elif(speak == 7):
        print("Touch my Back Head")
        talk.say("Touch my Back Head")
        Original_Movements_nao.append("3")

    return Original_Movements_nao

def Medium_Series(speak):
    print("Write medium series")
    talk = ALProxy("ALTextToSpeech")
    if(speak == 1):
        print("Touch my Right Leg and left leg")
        talk.say("Touch my Right Leg and Left Leg")
        Original_Movements_nao.append("7")
        Original_Movements_nao.append("6")
    elif(speak == 2):
        print("Touch my Fore Head and Left Leg")
        talk.say("Touch my Fore Head and Left Leg")
        Original_Movements_nao.append("1")
        Original_Movements_nao.append("6")
    elif(speak == 3):
        print("Touch my Right Leg and Back Head")
        talk.say("Touch my Right Leg and Back Head")
        Original_Movements_nao.append("7")
        Original_Movements_nao.append("3")
    elif(speak == 4):
        print("Touch my Back Head and Fore Head")
        talk.say("Touch my Back Head anf Fore Head")
        Original_Movements_nao.append("3")
        Original_Movements_nao.append("1")
    elif(speak == 5):
        print("Touch my Middle Head and Right Leg")
        talk.say("Touch my Middle Head and Right Leg")
        Original_Movements_nao.append("2")
        Original_Movements_nao.append("7")
    elif(speak == 6):
        print("Touch my Left Leg and Left Hand")
        talk.say("Touch my Left Leg and Left Hand")
        Original_Movements_nao.append("6")
        Original_Movements_nao.append("4")
    elif(speak == 7):
        print("Touch my Fore Head and Middle Head")
        talk.say("Touch my Fore Head and Middle Head")
        Original_Movements_nao.append("1")
        Original_Movements_nao.append("2")

    return Original_Movements_nao

def Hard_Series(speak):
    print("Write hard series")
    talk = ALProxy("ALTextToSpeech")
    if(speak == 1):
        print("Touch my Right Leg, Left Leg and Left Hand")
        talk.say("Touch my Right Leg, Left Leg and Left Hand")
        Original_Movements_nao.append("7")
        Original_Movements_nao.append("6")
        Original_Movements_nao.append("4")
    elif(speak == 2):
        print("Touch my Left Hand, Fore Head and Right Leg")
        talk.say("Touch my Left Hand, Fore Head and Right Leg")
        Original_Movements_nao.append("4")
        Original_Movements_nao.append("1")
        Original_Movements_nao.append("7")
    elif(speak == 3):
        print("Touch my Fore Head, Middle Head and Fore Head")
        talk.say("Touch my Fore Head, Middle Head and Fore Head")
        Original_Movements_nao.append("1")
        Original_Movements_nao.append("2")
        Original_Movements_nao.append("1")
    elif(speak == 4):
        print("Touch my Left Leg, Back Head and Right Leg")
        talk.say("Touch my Left Leg, Back Head and Right Leg")
        Original_Movements_nao.append("6")
        Original_Movements_nao.append("3")
        Original_Movements_nao.append("7")
    elif(speak == 5):
        print("Touch my Middle Head, Middle Head and Fore Head")
        talk.say("Touch my Middle Head, Middle Head and Fore Head")
        Original_Movements_nao.append("2")
        Original_Movements_nao.append("2")
        Original_Movements_nao.append("1")
    elif(speak == 6):
        print("Touch my Back Head, Right Leg and Middle Head")
        talk.say("Touch my Back Head, Right Leg and Middle Head")
        Original_Movements_nao.append("3")
        Original_Movements_nao.append("7")
        Original_Movements_nao.append("2")
    elif(speak == 7):
        print("Touch my Back Head, Left Leg and Left Leg")
        talk.say("Touch my Back Head, Left Leg and Left Leg")
        Original_Movements_nao.append("3")
        Original_Movements_nao.append("6")
        Original_Movements_nao.append("6")

    return Original_Movements_nao

def main():
    main_talk = ALProxy("ALTextToSpeech")
    main_talk.say("with" + "a")
    main_talk.say("easy one")
    time.sleep(1)
    Easy_Series(random.randint(1,7))

if __name__ == "__main__":
    main()
