import sys
sys.dont_write_bytecode = True
import time

from naoqi import ALProxy
from naoqi import ALBroker
from naoqi import ALModule


def speechReco():
    vocabulary = ["play","talk"]
    #variable to hold the user input
    User_input = []
    #language is set here
    speechRecoControl.setLanguage("English")
    #in-built pause fuction from naoqi
    speechRecoControl.pause(True)
    #main function which reads the vocabulary
    speechRecoControl.setVocabulary(vocabulary, False) #change to True
    #starting the speechReco
    speechRecoControl.subscribe(iptouse)
    #definiing memeory proxy to get the user input speak
    memProxy = ALProxy("ALMemory", iptouse, porttouse)
    memProxy.subscribeToEvent('WordRecognized', iptouse, 'wordRecognized')
    #in-built pause function from naoqi
    speechRecoControl.pause(False)

    #time-delay
    time.sleep(6)

    speechRecoControl.unsubscribe(iptouse)
    User_input = memProxy.getData('WordRecognized')
    memProxy.unsubscribeToEvent('WordRecognized', iptouse)
    return User_input[0]

def chatReco():
    chat_vocabulary = ["great","good","bad","ok","sunny","cold","windy","rainy","yes","no","coffee","beer","wine","tea"]
    #variable to hold the user User_input
    chat_User_input = []
    #language is set here
    speechRecoControl.setLanguage("English")
    #main function which reads the vocabulary
    speechRecoControl.pause(True)
    #main function which reads the vocabulary
    speechRecoControl.setVocabulary(chat_vocabulary, False) #change to True
    #starting the speechReco
    speechRecoControl.subscribe(iptouse)
    #definiing memeory proxy to get the user input speak
    chat_memProxy = ALProxy("ALMemory", iptouse, porttouse)
    chat_memProxy.subscribeToEvent('WordRecognized', iptouse, 'wordRecognized')
    #in-built pause function from naoqi
    speechRecoControl.pause(False)

    #time-delay
    time.sleep(8)
    speechRecoControl.unsubscribe(iptouse)
    chat_User_input = chat_memProxy.getData('WordRecognized')
    chat_memProxy.unsubscribeToEvent('WordRecognized', iptouse)
    return chat_User_input[0]


def speechReco_main(ip,port):

    myBroker_child_7 = ALBroker("myBroker",
       "0.0.0.0",
       0,
       ip,
       port)

    global iptouse
    iptouse = ip
    global porttouse
    porttouse = port

    global speechRecoControl
    speechRecoControl = ALProxy("ALSpeechRecognition")



if __name__ == "__main__":
    speechReco_main()
