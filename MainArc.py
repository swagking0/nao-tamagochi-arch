#python modules
import sys
sys.dont_write_bytecode = True
import time
import argparse
import random

#naoqi modules
from naoqi import ALProxy
from naoqi import ALBroker
from naoqi import ALModule

#Arch modules
import touch
import battery
import led
import armmotors
import posters
import speech
import speechReco
import GETandPush_Data


#----------------------------------Arch starts from here-----------------------------#
def Robot_ONSTART_INTERACTION():
    time.sleep(3)
    led.LED_color_eyes(LEDS,led.ColorList[5],1)
    armmotors.stiff_RHand(armMotorControl,1)
    armmotors.setang_body(armMotorControl,["RShoulderPitch","RElbowYaw","RElbowRoll"],[0.2, 1.2, 0.2])
    time.sleep(1.5)
    Interaction_1 = ["Hello I am Fender","I am happy to see you today","Lets enjoy by doing some cool stuff", "So, What shell we do?", "play or talk"]
    speech.talk(speekControl, Interaction_1[0])
    time.sleep(0.5)
    speech.talk(speekControl, Interaction_1[1])
    time.sleep(0.5)
    speech.talk(speekControl, Interaction_1[2])
    armmotors.setang_body(armMotorControl,["RShoulderPitch","RElbowYaw","RElbowRoll"],[0.8, 0.6, 0.8])
    led.LED_color_eyes(LEDS,led.ColorList[2],1)
    speech.talk(speekControl, Interaction_1[3])
    time.sleep(0.5)
    speech.talk(speekControl, Interaction_1[4])
    armmotors.stiff_RHand(armMotorControl,0)
    time.sleep(1)
    spechrec = speechReco.speechReco()

    if(spechrec == "play"):
        Robot_ONPLAYandONTALK(spechrec)
    elif(spechrec == "talk"):
        Robot_ONPLAYandONTALK(spechrec)
    else:
        Robot_ONPLAYandONTALK("False")

    return spechrec

def Robot_ONPLAYandONTALK(getValue):
    time.sleep(1)
    print(getValue)
    Interaction_2 = ["Awesome lets play my game reach to touch", "Awesome I love it", "So let me ask you a question" ,"I think you are not in mood to do something"]
    if(getValue == "talk"):
        GETandPush_Data.data_PUSH_talk(True)
        GETandPush_Data.data_PUSH_play(False)
        chat_Interaction = ["How was your day?","How is the weather outside?","Which drink you like most?"]
        speech.talk(speekControl, Interaction_2[1])
        speech.talk(speekControl, Interaction_2[2])
        time.sleep(1)
        chat_generate_speech = random.choice(chat_Interaction)
        if(chat_generate_speech == "Which drink you like most?"):
            speech.talk(speekControl, chat_generate_speech)
            time.sleep(1)
            speech.talk(speekControl,"coffee or tea or wine or beer")
        else:
            speech.talk(speekControl, chat_generate_speech)
        time.sleep(2)
        chat_spechrec = speechReco.chatReco()
        print(chat_spechrec)
        Robot_ONTALK(chat_spechrec)
    elif(getValue == "play"):
        GETandPush_Data.data_PUSH_play(True)
        GETandPush_Data.data_PUSH_talk(False)
        speech.talk(speekControl, Interaction_2[0])
        time.sleep(1)
        speech.talk(speekControl, "ok")
        speech.talk(speekControl, "let's start")
        touch.main()
        global ReactToTouch
        ReactToTouch = touch.ReactToTouch("ReactToTouch")
        time.sleep(10)
        touch.Level_2.append("False")
        touch.Level_3.append("False")
        del touch.Original_Movements_nao[:]
        del touch.Movements_nao[:]
        ReactToTouch.exit()
        time.sleep(2)
        if(touch.Level_2[0] == "True"):
            speech.talk(speekControl, "Awesome")
            speech.talk(speekControl, "now let us try")
            speech.talk(speekControl, "combinations")
            speech.talk(speekControl, "to make the game")
            speech.talk(speekControl, "more interesting")
            generator_int = random.randint(1,7)
            touch.Medium_Series(generator_int)
            global ReactToTouch_Level2
            ReactToTouch_Level2 = touch.ReactToTouch("ReactToTouch")
            time.sleep(10)
            del touch.Level_2[:]
            del touch.Level_3[:]
            ReactToTouch_Level2.exit()
            if(touch.Movements_nao != touch.Original_Movements_nao):
                del touch.Original_Movements_nao[:]
                del touch.Movements_nao[:]
                time.sleep(2)
                print("End of level_2")
                speech.talk(speekControl, "End of Level 2")
                time.sleep(2)
                speech.talk(speekControl, "that is bad")
                speech.talk(speekControl, "check with your")
                speech.talk(speekControl, "concentration")
                speech.talk(speekControl, "does not seems here")
                Robot_ONIDEAL()
            elif(touch.Movements_nao == touch.Original_Movements_nao):
                del touch.Original_Movements_nao[:]
                del touch.Movements_nao[:]
                touch.Level_3.append("True")
                time.sleep(2)
                if(touch.Level_3[0] == "True"):
                    speech.talk(speekControl, "Impressed")
                    speech.talk(speekControl, "but it is not the end")
                    speech.talk(speekControl, "here comes the final")
                    speech.talk(speekControl, "and")
                    speech.talk(speekControl, "interesting one")
                    generator_int = random.randint(1, 7)
                    touch.Hard_Series(generator_int)
                    global ReactToTouch_Level3
                    ReactToTouch_Level3 = touch.ReactToTouch("ReactToTouch")
                    time.sleep(10)
                    del touch.Level_2[:]
                    del touch.Level_3[:]
                    ReactToTouch_Level3.exit()
                    if(touch.Movements_nao != touch.Original_Movements_nao):
                        del touch.Original_Movements_nao[:]
                        del touch.Movements_nao[:]
                        time.sleep(2)
                        print("End of level_3")
                        speech.talk(speekControl, "End of Level 3")
                        time.sleep(2)
                        speech.talk(speekControl, "I would say")
                        speech.talk(speekControl, "I am impressed")
                        speech.talk(speekControl, "but still")
                        speech.talk(speekControl, "you failed")
                        speech.talk(speekControl, "and")
                        speech.talk(speekControl, "disappointed me")
                        speech.talk(speekControl, "little")
                        speech.talk(speekControl, "I hope next time")
                        speech.talk(speekControl, "you do not do this")
                        Robot_ONIDEAL()
                    elif(touch.Movements_nao == touch.Original_Movements_nao):
                        time.sleep(2)
                        speech.talk(speekControl, "congratulations")
                        speech.talk(speekControl, "you completed")
                        speech.talk(speekControl, "all the levels")
                        speech.talk(speekControl, "of my game")
                        speech.talk(speekControl, "succefully")
                        Robot_ONIDEAL()
        else:
            del touch.Level_2[:]
            del touch.Level_3[:] # Important to check - Multiple times
            del touch.Original_Movements_nao[:]
            del touch.Movements_nao[:]
            time.sleep(2)
            speech.talk(speekControl, "End of Level 1")
            time.sleep(2)
            speech.talk(speekControl, "I am not impressed")
            speech.talk(speekControl, "let us come back")
            speech.talk(speekControl, "and try again")
            print("End of level_1")
            Robot_ONIDEAL()
    elif(getValue == "False"):
        GETandPush_Data.data_PUSH_play(False)
        GETandPush_Data.data_PUSH_talk(False)
        speech.talk(speekControl, Interaction_2[3])
        time.sleep(2)
        Robot_STATUS()

def Robot_ONPLAY():
    GETandPush_Data.data_PUSH_play(True)
    GETandPush_Data.data_PUSH_talk(False)
    speech.talk(speekControl, "Ok")
    speech.talk(speekControl, "So")
    speech.talk(speekControl, "let's start")
    touch.main()
    global ReactToTouch
    ReactToTouch = touch.ReactToTouch("ReactToTouch")
    time.sleep(10)
    touch.Level_2.append("False")
    touch.Level_3.append("False")
    del touch.Original_Movements_nao[:]
    del touch.Movements_nao[:]
    ReactToTouch.exit()
    time.sleep(2)
    if(touch.Level_2[0] == "True"):
        speech.talk(speekControl, "Awesome")
        speech.talk(speekControl, "now let us try")
        speech.talk(speekControl, "combinations")
        speech.talk(speekControl, "to make the game")
        speech.talk(speekControl, "more interesting")
        generator_int = random.randint(1,7)
        touch.Medium_Series(generator_int)
        global ReactToTouch_Level2
        ReactToTouch_Level2 = touch.ReactToTouch("ReactToTouch")
        time.sleep(10)
        del touch.Level_2[:]
        del touch.Level_3[:]
        ReactToTouch_Level2.exit()
        if(touch.Movements_nao != touch.Original_Movements_nao):
            del touch.Original_Movements_nao[:]
            del touch.Movements_nao[:]
            touch.Level_3.append("True")
            time.sleep(2)
            print("End of Level_2")
            speech.talk(speekControl, "End of Level 2")
            time.sleep(2)
            speech.talk(speekControl, "that is bad")
            speech.talk(speekControl, "check with your")
            speech.talk(speekControl, "concentration")
            speech.talk(speekControl, "does not seems here")
            Robot_ONIDEAL()
        elif(touch.Movements_nao == touch.Original_Movements_nao):
            del touch.Original_Movements_nao[:]
            del touch.Movements_nao[:]
            touch.Level_3.append("True")
            time.sleep(2)
            if(touch.Level_3[0] == "True"):
                speech.talk(speekControl, "Impressed")
                speech.talk(speekControl, "but it is not the end")
                speech.talk(speekControl, "here comes the final")
                speech.talk(speekControl, "and")
                speech.talk(speekControl, "interesting one")
                generator_int = random.randint(1,7)
                touch.Hard_Series(generator_int)
                global ReactToTouch_Level3
                ReactToTouch_Level3 = touch.ReactToTouch("ReactToTouch")
                time.sleep(10)
                del touch.Level_2[:]
                del touch.Level_3[:]
                ReactToTouch_Level3.exit()
                if(touch.Movements_nao != touch.Original_Movements_nao):
                    del touch.Original_Movements_nao[:]
                    del touch.Movements_nao[:]
                    time.sleep(2)
                    print("End of Level_3")
                    speech.talk(speekControl, "End of Level 3")
                    time.sleep(2)
                    speech.talk(speekControl, "I would say")
                    speech.talk(speekControl, "I am impressed")
                    speech.talk(speekControl, "but still")
                    speech.talk(speekControl, "you failed")
                    speech.talk(speekControl, "and")
                    speech.talk(speekControl, "disappointed me")
                    speech.talk(speekControl, "little")
                    speech.talk(speekControl, "I hope next time")
                    speech.talk(speekControl, "you do not do this")
                    Robot_ONIDEAL()
                elif(touch.Movements_nao == touch.Original_Movements_nao):
                    print("You completed all the levels succefully!!!")
                    print("Congratulations")
                    time.sleep(2)
                    speech.talk(speekControl, "congratulations")
                    speech.talk(speekControl, "you completed")
                    speech.talk(speekControl, "all the levels")
                    speech.talk(speekControl, "of my game")
                    speech.talk(speekControl, "succefully")
                    Robot_ONIDEAL()
    else:
        del touch.Level_2[:]
        del touch.Level_3[:] # Important to check - Multiple times
        del touch.Original_Movements_nao[:]
        del touch.Movements_nao[:]
        time.sleep(2)
        speech.talk(speekControl, "End of Level 1")
        time.sleep(2)
        speech.talk(speekControl, "I am not impressed")
        speech.talk(speekControl, "let us come back")
        speech.talk(speekControl, "and try again")
        print("End of Level_1")
        Robot_ONIDEAL()


def Robot_ONTALK(getValue_chat):
    #["great","good","bad","ok","sunny","cold","windy","rainy","yes","no","coffee","beer","wine","tea"]
    #how is ur day
    if(getValue_chat == "great"):
        speech.talk(speekControl, "Your tone shows the positiveness")
        speech.talk(speekControl, "hope it ends and stays the same")
        speech.talk(speekControl, "with that positiveness")
        speech.talk(speekControl, "let's play a small game")
        time.sleep(1)
        Robot_ONPLAY()
    elif(getValue_chat == "good"):
        speech.talk(speekControl, "I know that")
        speech.talk(speekControl, "you are always positive")
        speech.talk(speekControl, "together let's make this day great")
        speech.talk(speekControl, "by playing a small game")
        time.sleep(1)
        Robot_ONPLAY()
    elif(getValue_chat == "ok"):
        speech.talk(speekControl, "look's like")
        speech.talk(speekControl, "your day is unstable")
        speech.talk(speekControl, "do not worry")
        speech.talk(speekControl, "I am here")
        speech.talk(speekControl, "to make your day end great")
        speech.talk(speekControl, "for which")
        speech.talk(speekControl, "let's play my favourite game")
        time.sleep(1)
        Robot_ONPLAY()
    elif(getValue_chat == "bad"):
        speech.talk(speekControl, "I can understand")
        speech.talk(speekControl, "you are tune look's down")
        speech.talk(speekControl, "remember")
        speech.talk(speekControl, "life is full of good and bad surprises")
        speech.talk(speekControl, "so just think")
        speech.talk(speekControl, "today was bad suprise")
        speech.talk(speekControl, "but don't worry")
        speech.talk(speekControl, "I will make it right")
        speech.talk(speekControl, "for that you need to play")
        speech.talk(speekControl, "small game with me")
        time.sleep(1)
        Robot_ONPLAY()
    ###
    #weather outside
    elif(getValue_chat == "sunny"):
        speech.talk(speekControl, "I love sunny day")
        speech.talk(speekControl, "I feel like")
        speech.talk(speekControl, "going out to play")
        speech.talk(speekControl, "but don't worry")
        speech.talk(speekControl, "I have a cool idea")
        speech.talk(speekControl, "let's play my game reach to touch")
        time.sleep(1)
        Robot_ONPLAY()
    elif(getValue_chat == "cold"):
        speech.talk(speekControl, "cold...")
        speech.talk(speekControl, "I think")
        speech.talk(speekControl, "you should suit up")
        speech.talk(speekControl, "just like ironman")
        speech.talk(speekControl, "don't worry")
        speech.talk(speekControl, "let's play a game")
        speech.talk(speekControl, "still your suit is ready")
        time.sleep(1)
        Robot_ONPLAY()
    elif(getValue_chat == "windy"):
        speech.talk(speekControl, "I sugest")
        speech.talk(speekControl, "you")
        speech.talk(speekControl, "not to go outside")
        speech.talk(speekControl, "still it turns normal")
        speech.talk(speekControl, "In the mean time")
        speech.talk(speekControl, "let's play a small game")
        time.sleep(1)
        Robot_ONPLAY()
    elif(getValue_chat == "rainy"):
        speech.talk(speekControl, "thank you")
        speech.talk(speekControl, "for the valuable information")
        speech.talk(speekControl, "my motors")
        speech.talk(speekControl, "don't like water")
        speech.talk(speekControl, "If feel")
        speech.talk(speekControl, "even your body too")
        speech.talk(speekControl, "don't worry")
        speech.talk(speekControl, "we can play small game")
        speech.talk(speekControl, "still it stops")
        time.sleep(1)
        Robot_ONPLAY()
    ###
    #favourite drink
    elif(getValue_chat == "coffee") or (getValue_chat == "beer") or (getValue_chat == "wine") or (getValue_chat == "tea"):
        speech.talk(speekControl, "So you like")
        speech.talk(speekControl, getValue_chat)
        speech.talk(speekControl, "my favourite")
        speech.talk(speekControl, "is Electricity")
        speech.talk(speekControl, "So")
        speech.talk(speekControl, "let's play my game")
        speech.talk(speekControl, "stress ourself's")
        speech.talk(speekControl, "and then grab")
        speech.talk(speekControl, "our favourite drinks")
        time.sleep(1)
        Robot_ONPLAY()
    ###
    else:
        speech.talk(speekControl, "I feel")
        speech.talk(speekControl, "you do not")
        speech.talk(speekControl, "like to")
        speech.talk(speekControl, "talk with me")
        time.sleep(1)
        Robot_ONIDEAL()


def Robot_ONIDEAL():
    GETandPush_Data.data_PUSH_play(False)
    GETandPush_Data.data_PUSH_talk(False)
    BatterValue = battery.battery_status()
    GETandPush_Data.data_PUSH(BatterValue)
    time.sleep(3)
    speech.talk(speekControl,"What you feel now.")
    speech.talk(speekControl,"Shell we have")
    speech.talk(speekControl, "a small talk")
    speech.talk(speekControl,"or")
    speech.talk(speekControl,"play my game?")
    time.sleep(6)
    spechrec = speechReco.speechReco()
    print(spechrec)
    if(spechrec == "play"):
        Robot_ONPLAYandONTALK("play")
    elif(spechrec == "talk"):
        Robot_ONPLAYandONTALK("talk")
    else:
        Robot_ONPLAYandONTALK("False")


def Robot_STATUS():
    if(GETandPush_Data.GET_happiness_factor() == "VERYSAD") and (GETandPush_Data.GET_health_factor() == "SICK"):
        speech.talk(speekControl, "I feel like I am going to die")
        #create animaition to show the behaviour
        armmotors.stiff_head(armMotorControl,1)
        armmotors.stiff_RHand(armMotorControl,1)
        armmotors.stiff_LHand(armMotorControl,1)
        body_varaibles = ["HeadYaw","HeadPitch"]
        body_varaibles_angles = [1.0 , 2.086017]
        ###
        armmotors.setang_body(armMotorControl,body_varaibles,body_varaibles_angles)
        led.LED_intensity_eyes(LEDS,0.3)
        led.LED_ear_OFF(LEDS)
        led.LED_head_OFF(LEDS)
        armmotors.closeHand_LHand(armMotorControl)
        armmotors.closeHand_RHand(armMotorControl)
        ###
        time.sleep(3)
        body_varaibles_angles = [1.0 , 0.0]
        armmotors.setang_body(armMotorControl,body_varaibles,body_varaibles_angles)
        led.LED_intensity_eyes(LEDS,0.3)
        led.LED_ear_ON(LEDS)
        led.LED_head_ON(LEDS)
        time.sleep(3)
        speech.talk(speekControl, "Look at my happiness and health factors.")
        speech.talk(speekControl, "They look too unstable for me to survive.")
        armmotors.stiff_head(armMotorControl,0)
        armmotors.stiff_RHand(armMotorControl,0)
        armmotors.stiff_LHand(armMotorControl,0)
        Robot_ONIDEAL()
    elif(GETandPush_Data.GET_happiness_factor() == "SAD") and (GETandPush_Data.GET_health_factor() == "UNHEALTHY"):
        speech.talk(speekControl, "I feel like, you do not like to take care of me.")
        #create animaition to show the behaviour
        armmotors.stiff_head(armMotorControl,1)
        body_varaibles = ["HeadYaw","HeadPitch"]
        body_varaibles_angles = [1.0 , -2.086017]
        ###
        led.LED_fade_eyes(LEDS,0.2,5)
        armmotors.setang_body(armMotorControl,body_varaibles,body_varaibles_angles)
        led.LED_fade_ear(LEDS,0.2,5)
        led.LED_fade_head(LEDS,0.2,5)
        ###
        time.sleep(3)
        body_varaibles_angles = [1.0 , 0.0]
        led.LED_fade_eyes(LEDS,0.2,5)
        armmotors.setang_body(armMotorControl,body_varaibles,body_varaibles_angles)
        led.LED_fade_ear(LEDS,0.2,5)
        led.LED_fade_head(LEDS,0.2,5)
        time.sleep(3)
        speech.talk(speekControl,"Just look at my happiness and health factor.")
        speech.talk(speekControl,"They looks unstable.")
        armmotors.stiff_head(armMotorControl,0)
        Robot_ONIDEAL()
    elif(GETandPush_Data.GET_food_factor() == "VERYHUNGRY"):
        speech.talk(speekControl, "I feel like rats running in my stomach.")
        #create hand movement
        armmotors.stiff_RHand(armMotorControl,1)
        body_varaibles = ["RShoulderPitch","RElbowYaw","RElbowRoll"]
        body_varaibles_angles = [0.4,-0.35,1.6]
        armmotors.setang_body(armMotorControl,body_varaibles,body_varaibles_angles)
        time.sleep(3)
        body_varaibles_angles = [0.8,0.2,0.8]
        armmotors.setang_body(armMotorControl,body_varaibles,body_varaibles_angles)
        time.sleep(3)
        armmotors.stiff_RHand(armMotorControl,0)
        time.sleep(3)
        speech.talk(speekControl,"Electricity is my favourite.")
        speech.talk(speekControl,"Just a clue.")
        Robot_ONIDEAL()
    elif(GETandPush_Data.GET_energy_factor() == "NORMAL") or (GETandPush_Data.GET_health_factor() == "NORMAL") or (GETandPush_Data.GET_happiness_factor() == "NORMAL") or (GETandPush_Data.GET_food_factor() == "NORMAL"):
        Interaction_4 = ["Look at the my application. To know what I am feeling","Somtimes I feel more intelligent than you","Hey look at me. Do I look handsome","It seems you like to take care of me. Some of my factors looks stable"]
        generate_speech = random.choice(Interaction_4)
        speech.talk(speekControl, generate_speech)
        time.sleep(3)
        Robot_ONIDEAL()



def main(ip, port):
    """ Main entry point
    """
    # We need this broker to be able to construct
    # NAOqi modules and subscribe to other modules
    # The broker must stay alive until the program exists
    global myBroker_Main
    myBroker_Main = ALBroker("myBroker",
       "0.0.0.0",   # listen to anyone
       0,           # find a free port and use it
       ip,          # parent broker IP
       port)        # parent broker port

    global ip_to
    global port_to
    ip_to = ip
    port_to = port

    ########################################################################
    #touch.main(ip_to,port_to)
    battery.battery_main(ip_to,port_to)
    led.led_main(ip_to,port_to)
    armmotors.armMotor_main(ip_to,port_to)
    posters.posters_main(ip_to,port_to)
    speech.speech_main(ip_to,port_to)
    speechReco.speechReco_main(ip_to,port_to)
    ########################################################################
    #------Global variables----------------#
    #ALModule variables
    global BatterValue
    global LEDS
    global armMotorControl
    global postersControl
    global speekControl
    global AutonomousLife
    #database variables
    global EnergyValue
    global HealthValue
    global HappinessValue
    global FoodValue
    ########################################################################
    #----------proxy variables----------------------------#
    #ReactToTouch = touch.ReactToTouch("ReactToTouch")
    LEDS = ALProxy("ALLeds")
    armMotorControl = ALProxy("ALMotion")
    postersControl = ALProxy("ALRobotPosture")
    speekControl = ALProxy("ALTextToSpeech")
    AutonomousLife = ALProxy("ALAutonomousLife")
    ########################################################################

    ##################################Implementation Area###########################################
    Robot_ONSTART_INTERACTION()

    try:
        while True:
            time.sleep(1)
            #BatterValue = battery.battery_status()
            #GETandPush_Data.data_PUSH(BatterValue)
    except KeyboardInterrupt:
        print("Interrupted by user, shutting down")
        myBroker_Main.shutdown()
        sys.exit(0)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="127.0.0.1", help="Robot ip address")
    parser.add_argument("--port", type=int, default=9559, help="Robot port number")
    args = parser.parse_args()
    main(args.ip, args.port)
