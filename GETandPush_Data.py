#python module
import pyrebase
import time
import sys
sys.dont_write_bytecode = True

########################################--Definition Area--########################################
config = {
    "apiKey": "AIzaSyDgDe6_EyfhI2zS4Wnb3UCHUV4vzMtBOkg",
    "authDomain": "tamagotchi-263ad.firebaseapp.com",
    "databaseURL": "https://tamagotchi-263ad.firebaseio.com",
    "storageBucket": "tamagotchi-263ad.appspot.com"
}

# '''
# 	initialize app with config
# '''
firebase = pyrebase.initialize_app(config)
# '''
# 	authenticate a user
# '''
auth = firebase.auth()
user = auth.sign_in_with_email_and_password("balavivek107@gmail.com", "123456789")


db = firebase.database()
####################################################################################################

#function to PUSH value
def data_PUSH(battery_values):
    db.child("agents").child("Lana").update({"battery_value": str(battery_values)}, user['idToken'])

def data_PUSH_talk(totalk):
    db.child("agents").child("Lana").update({"chatting": str(totalk)}, user['idToken'])

def data_PUSH_play(toplay):
    db.child("agents").child("Lana").update({"playing": str(toplay)}, user['idToken'])

#functions to GET value
def GET_energy_factor():
    energy_factor = db.child("agents").child("Lana").child("energyfactor").get(user['idToken']).val()
    return energy_factor

def GET_health_factor():
    health_factor = db.child("agents").child("Lana").child("healthfactor").get(user['idToken']).val()
    return health_factor

def GET_happiness_factor():
    happiness_factor = db.child("agents").child("Lana").child("happinessfactor").get(user['idToken']).val()
    return happiness_factor

def GET_food_factor():
    food_factor = db.child("agents").child("Lana").child("foodfactor").get(user['idToken']).val()
    return food_factor

#test main -- not to used in project arch
def database_main():

    try:
        while True:
            time.sleep(1)
            check_energy = GET_energy_factor()
            check_health = GET_health_factor()
            check_happiness = GET_happiness_factor()
            check_food = GET_food_factor()
            print("This is the energy value from data base" + " : " +check_energy)
            print("This is the health value from data base" + " : " +check_health)
            print("This is the happiness value from data base" + " : " +check_happiness)
            print("This is the food value from data base" + " : " +check_food)
    except KeyboardInterrupt:
        print("Interrupted by user, shutting down")
        sys.exit(0)




if __name__ == "__main__":
    database_main()
    
